Slime World
===========

_Slime World_ is an MSX2 game (128K VRAM required). It appeared on the disk
that came with the January 1993 issue of [_MSX・FAN_][msxfan] magazine. The
story and instructions for playing the game (遊び方) are on page 31 and
technical commentary (解説) and note from the programmer appear at the
bottom of page 55. Both scanned pages are in [`Slime World (MSXFAN
1993-01).pdf`][msxfan sw], extracted from the [full scan][msxfan 1993-01]
on archive.org.

### p.34 遊び方 / Gameplay Instructions

(Not yet translated.)

### p.55 解説 / Technical Commentary

- `SLIMESTA.FD1` [sic; actually `.BAS`] Screen mode setup; runs `SLIMOPNG.SLM`.
- `SLIMOPNG.SLM` Opening BASIC program. Runs the main program.
  - `TITLGRPC.SLM`: Opening graphic.
  - `BGM-PSGV.SLM`: PSG BGM data.
  - `BGM-FMVS.SLM`: FM sound BGM data. (The main program's sound [PSG or FM]
     is set up in the opening program.)
  - `FMSLOTCK.SLM`: Machine language program to check presence of FM sound.
- `SLIMMAIN.SLM`: Main BASIC program.
  - `SCROLLMC.SLM`: Machine language file for scrolling.
  - `CRCTDATA.SLM`: Character pattern data.
  - `SPRTDATA.SLM`: Sprite pattern data.
  - `DATAMAP0.SLM`-`DATAMAP4.SLM`: Map data.
  - `DATAMSD0.SLM`-`DATAMSD4.SLM`: Monster data.
- `SLIMENDP.SLM`: Ending program.
  - `SLIMENDG.SLM`: Ending graphic data.
  - `END-PSGV.SLM`: PSG ending BGM data.
  - `END-FMVS.SLM`: FM ending BGM data.
- `SAVEDAT0.SLM`-`SAVEDAT2.SLM`: Saves 1 through 3 data. Created on first save.

Notes:
- "PSG" (Programmable Sound Generator) is the
  [General Instrument AY-3-8910][gi] sound chip.
- "BGM" is background music.


Running and Debugging
---------------------

- During the title screen (`SLIMOPNG.SLM`), pressing the button will start
  the game, but holding Shift while you press the button will start the
  music browser.
- `SLIMOPNG.SLM` must always be run before `SLIMMAIN.SLM` because the
  former does essential setup for the latter.
- Ctrl-STOP during the game will leave the screen in the game graphics
  character set; see [the top-level README](../README.md) for more on how
  to deal with this. In this game's charset most upper-case romaji and
  numbers are standard; most lower-case and punctuation are game graphics.


Theory of Operation
-------------------

### Files

`SLIMESTA.BAS` (called `SLIMESTA.FD1` in the Technical Commentary) is
intended to be the entry point but seems trivial. It just runs the actual
setup/opening screen program, `SLIMOPNG.SLM`, which can be run directly and
re-does all setup done here.

The highest-level loop is through `SLIMOPNG.SLM`, which does essential
setup for the other BASIC programs and the opening screen and music.
Pressing the button or spacebar runs `SLIMMAIN.SLM`, the game itself.
`SLIMENDP.SLM` is the ending program after the game is complete; this runs
`SLIMOPNG.SLM` to start a new game.

The BASIC programs read the following additional files:

    Filename         Line  T  strt  end  entry  notes
    ───────────────────────────────────────────────────────────────────
    SLIMOPNG.SLM
      TITLGRPC.SLM     20  S  0000  6a00  0000
      FMSLOTCK.SLM   5020     C800  C8A2  C800   USR=$C800
      BGM-FMVS.SLM   5050  R  9DAF  CCCE  9DAF  USR3=$DE10
      BGM-PSGV.SLM   5060  R  C1AF  D9B1  C1AF  USR3=$DE00
     †CRCTDATA.SLM  20000  S  0000  37FF  0000
    ───────────────────────────────────────────────────────────────────
    SLIMMAIN.SLM
     †CRCTDATA.SLM     20  S  0000  37FF  0000
      SPRTDATA.SLM     40  S  3800  3FFF  0000
      SCROLLMC.SLM    370     DD00  DD40  DD00   USR=$DD00
      DATAMAPn.SLM    900  S  4000  7FFF  4000
      DATAMSDn.SLM    920  A
    ───────────────────────────────────────────────────────────────────
    SLIMENDG.SLM
      SLIMENDG.SLM     10  S  0000  6a00  0000
      END-FMVS.SLM   5020  R  9DAF  AA7C  9DAF  USR1=$DE10
      END-PSGV.SLM   5040  R  C1AF  C77C  C1AF  USR1=$DE00

- `T` (type) indicates file type/`BLOAD` options:
  - `A`: ASCII data file read with `OPEN`.
  - `R`: Binary file; start address called after loading.
  - `S`: Binary file; loaded to VRAM.
- † marks files loaded by more than one BASIC program.
- `n` is `0` through `4` (probably level or area number).

All binary files are extended to a multiple of 128 bytes with $1A (Ctrl-Z)
characters padding at the end. These extra data are not loaded by `BLOAD`.

### Memory Usage

- opng:
  -  5000: `CLEAR  200, &h9DA0`: extra space used during setup only?
  -  5070: `CLEAR  300, &hDCFF`: return extra space after setup
  - 20000: `CLEAR 1000, &hDCFF`: Music Browser does lots of string manip.
  - 20510: `CLEAR  300, &hDCFF`: before return to Intro Screen
- main:
  -    20: `CLEAR 1000, &HDCFF`
- endp:
  -  5000: `CLEAR  200, &H9DA0`
  -  5020: `CLEAR  300, &HDCFF`
  -  5040: `CLEAR  300, &HDCFF`

Locations:

- `$DE3D` (3b): SLIMOPNG.SLM stores `"PSG"` or `"FM "` depending on sound
  selection. Read by SLIMENDP.SLM so it can load PSG or FM ending music.

### USR Assignments

main:

      USR   addr  line calls  notes
    ──────────────────────────────────────────────────────────────────────
    SLIMOPNG.SLM:
      USR   $C800  5020  2    FMSLOTCK.SLM
      USR3  $DE10  5050  6†   when FM sound selected
      USR3  $DE00  5060  6†   when PSG sound selected
    ──────────────────────────────────────────────────────────────────────
    SLIMMAIN.SLM:
      USR   $007E    20  1    SETGRP: GRAPHICS 2 (SCREEN 2) mode
      USR   $DD00   370  3
      USR1  $0041    20  1    DISSCR: inhibit screen display
      USR2  $0044    20  1    ENASCR: enable screen display
      USR3  (inh.)‡     13    BGM track n, 255=off
    ──────────────────────────────────────────────────────────────────────
    SLIMENDP.SLM:
      USR   $003E  5000       INIFNK: Initialize function key contents
      USR   $0000  5000  1    RESET: Reboots machine
      USR1  $DE10  5020  2†   when FM sound selected
      USR1  $DE00  5040  2†   when PSG sound selected

- † All calls to USR3/USR1 are to just one of the two that was selected.
- ‡ slmmain.slm `USR3` definition is inherited from slimopng.slm:
  $DE00 for PSG or $DE10 for FM.


<!-------------------------------------------------------------------->
[gi]: https://en.wikipedia.org/wiki/General_Instrument_AY-3-8910
[msxfan]: https://ja.wikipedia.org/wiki/MSX%E3%83%BBFAN
[msxfan 1993-01]: https://archive.org/details/msx-fan-magazine-1993-01
[msxfan sw]: ./Slime%20World%20(MSXFAN%201993-01).pdf
