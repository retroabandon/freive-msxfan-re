;
;   msx.inc - MSX system definitions

;     bit     76543201  Slot descriptor (slotdesc)
;     value   FxxxSSPP
;             │   │ └── primary slot number 0-3
;             │   └──── secondary slot number 0-3, or `xx` if not specified
;             └──────── 1 if secondary slot specified

; ----------------------------------------------------------------------
;   System calls

KEYINT      equ $038        ; timer interrupt handler; keyboard scan etc.

RDSLT       equ $00C        ; DI Read byte from slot: A=slotdesc/retval HL=addr
CALSLT      equ $01C        ; DI and inter-slot call; IX=addr, IY(hi)=slotdesc
ENASLT      equ $024        ; DI and map A=slotdesc to page containing addr HL
GICINI      equ $090        ; init PSG and static data for PLAY stmt
WRTPSG      equ $093        ; write data in E to PSG reg in A

PSG_C_AMP   equ 10          ; PSG channel C amplitude register

CHGSND      equ $135        ; 1-bit sound port status: A: 0=off ~0=on

; ----------------------------------------------------------------------
;   FM-BIOS and YM2413 OPLL

WRTOPL      equ $4110       ; write to OPLL register A=register E=data
INIOPL      equ $4113       ; init OPLL  HL=work area (word aligned)
RDDATA      equ $411C       ; read instrument data A=instno HL=buffer(8b)

OPLL_rhythm equ $0E         ; rhythm control register
                            ;     b5: 0=9 voice, 1=rhythm mode
                            ;   b4-0: BD,SD,TT,TC,HH enable

;   Each voice of the YM2413 has three registers to configure it.
;   There are 9 voices in tone mode (base to base+8),
;   and 6 tone voices in rhythm mode (base to base+5).
OPLL_vfnum   equ $10        ;       low 8 bits of note frequency
OPLL_vconf   equ $20        ; b7-6: unused
                            ;   b5: sustain on/off: 0=decay 1=sustain
                            ;   b4: key on/off: 0=mute 1=play
                            ; b1-3: octave
                            ;   b0: high bit of note frequency
OPLL_vinst   equ $30        ; b7-4: instrument (see below)
                            ; b3-0: volume (0-15)

;   Rhythm mode definitions: voices 7-9 (r_fnum to r_fnum+2) are
;   specially configured per vendor instructions.
OPLL_rfnum    equ $16       ; base of 3 F-number regs for rhythm
OPLL_rfnval0  equ $20       ; vendor values
OPLL_rfnval1  equ $50
OPLL_rfnval2  equ $C0
OPLL_rconf    equ $26       ; base of 3 voice config regs for rhythm
OPLL_rcval0   equ $05       ; vendor values
OPLL_rcval1   equ $05
OPLL_rcval2   equ $01
;   Rhythm mode drum kit volumes
OPLLrvol_uubd equ $36       ; b7-4: unused   b3-0: base drum
OPLLrvol_hhsd equ $37       ; b7-4: hi-hat   b3-0: snare
OPLLrvol_tttc equ $38       ; b7-4: tom-tom  b3-0: top cymbal

;   Instrument values:
;       $0 original   $4 flute      $8 organ        $C vibraphone
;       $1 violin     $5 clarinet   $9 horn         $D synth bass
;       $2 guitar     $6 oboe       $A synthesizer  $F acoustic bass
;       $3 piano      $7 trumpet    $B harpsichord  $F elec guitar/bass

; ----------------------------------------------------------------------
;   Housekeeping data

RAMAD1      equ $F342       ; slotdesc of RAM in page 1 (DOS)

;   On a USR(x) call, A register and usrarg_type contain $02 if the
;   argument _x_ is a 16-bit signed integer and usrarg_int contains the
;   value. usrarg_type is also used to set the type of the return value.
usrarg_type equ $F663           ; 2=int%, 4=float(1) 8=float(#) 3=string
usrarg_int  equ $F7F8

EXPTBL      equ $FCC1       ; slot 0 expansion mode / main BIOS ROM slot addr

;   Timer interrupt hook; default $C9 × 5 (ret instructions).
;   This is called by the ROM 60 times per second
H.TIMI      equ $FD9F

