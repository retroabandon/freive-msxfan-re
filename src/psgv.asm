;
;   psgv.asm - PSG player code from Slime World
;
;   This is the common player code (and apparenty some common data) for the
;   game and end sequence. The sound data is expected to be at the
;   `tracktab` symbol, usually appended to this code.
;
;   For details about the `tracktab` format, see the comments after
;   `curtrack` at the bottom of this file.
;

            phase    $5200

p1dst
            ld  a,(usrarg_int)  ; assume USR(nn%) (integer argument)
            cp  $FE             ; tracknum = 254 ?
            jr  z,retplaying    ;   yes: return playing status
            ld  (tracknum),a    ;    no: save argument and fallthrough

            ;   Start playing track number (tracknum)
            ;   or stop BGM if tracknum is 255.
playtrack
            ld  a,(tracknum)    ; load previous USR(n) argument
            cp  $FF             ; tracknum = 255 ?
            jr  z,stopbgm       ; yes: stop playback and return
            ;
            ;   The track table is an array of 11-byte entries. Find the
            ;   offset to the entry and copy the entry to curtrack.
            ld  b,a             ; a ← a * 11
            sla a
            sla a
            sla a
            add a,b
            add a,b
            add a,b
            ld  b,$00           ; bc ← a
            ld  c,a
            ld  hl,tracktab
            add hl,bc           ; hl ← tracktab + (tracknum * 11)
            ld  de,curtrack
            ld  bc,11
            ldir                ; curtrack ← fresh copy of requested track entry

            ld  a,1             ; next PSG update will be at next tick
            ld  (wait),a

            ld  a,(playing)     ; 1 if we're playing
            or  a               ; set flags
            ret nz              ; if already playing, we're done

            ;   Set up timer hook to call player routine
            ld  hl,H.TIMI       ; save previous timer hook
            ld  de,savedhook
            ld  bc,5
            ldir
            ld  a,$C3           ; `jp nnnn` instruction
            ld  (H.TIMI),a
            ld  hl,bgm_tick
            ld  (H.TIMI+1),hl
            ld  a,$01           ; playing ← true
            ld  (playing),a
            ret

stopbgm     ld  a,(playing)     ; 1 if we're playing
            or  a               ; set flags
            ret z               ; if not playing, we're done

            xor a               ; A ← 0
            ld  (playing),a     ; indicate not playing
            ld  hl,savedhook    ; restore previous timer routine
            ld  de,H.TIMI
            ld  bc,5
            ldir
            call GICINI         ; init PSG
            ret

            ;   Return to USR() 1 if we're playing, 0 if we're stopped.
retplaying  ld  a,(playing)
            ld  (usrarg_int),a
            ret

bgm_update  ld  a,(wait)        ; reduce number of ticks to wait until
            dec a               ;   next PSG update
            ld  (wait),a
            ret nz              ; if still ticks to wait, return
            ;
            ld  a,(t_tempo)     ; load track tempo
            ld  (wait),a        ; set ticks to wait after this update
            ;
            ld  hl,(t_noteidxA) ; load current note to play, which is a
            ld  a,(hl)          ;   word index into notetab
            inc hl              ; move pointer to next note for next update
            ld  (t_noteidxA),hl
            cp  $FF             ; is the note index an end of track indicator?
            jr  z,next_trackA   ;   yes: go start next track (id in next byte)
            ;
            sla a               ; word index a * 2 gives us a byte
            ld  c,a             ;   which is LSB of addr in table
            ld  b,$00           ; MSB in table always $00
            ld  hl,notetab
            add  hl,bc          ; HL ← notetab + offset BC for address of
            ld  a,(hl)          ;   2-byte tone period from notetab
            ld  e,a             ; tone period LSB
            ld  a,0             ; R0: ch.A period LSB
            call WRTPSG
            ;
            inc hl              ; move to tone period MSB
            ld  a,(hl)
            ld  e,a
            ld  a,1             ; R1: ch.A period MSB
            call WRTPSG
            ;
            ld  a,(t_ampA)     ; load channel A amplitude
            ld  e,a
            ld  a,8             ; R8: ch.A amplitude (xxxMLLLL)
            call WRTPSG
            ;
            jr  bgm_udpateB     ; skip over next_trackA, which is here because
                                ;   it needs to be within 128 bytes of jr above

next_trackA ld  a,(hl)          ; load next track number
            ld  (tracknum),a    ; set as current track and restart player
            jp  playtrack

bgm_udpateB ld  hl,(t_noteidxB) ; substantially the same as bgm_update
            ld  a,(hl)          ;   except where noted below
            inc hl
            ld  (t_noteidxB),hl
            cp  $FF             ; if noteidxB table ends earlier than A, it
            jr  z,next_trackB   ;   determines the next track
            ;
            sla a
            ld  c,a
            ld  b,$00
            ld  hl,notetab
            add hl,bc
            ld  a,(hl)
            ld  c,a             ; C ← tone period LSB
            inc hl
            ld  a,(hl)
            ld  b,a             ; B ← tone period MSB
            ld  a,(flatB)
            ld  l,a
            ld  h,$00
            add hl,bc           ; increase period by flatB to detune it down
            ld  e,l
            ld  a,2             ; R2: ch.B period LSB
            call WRTPSG
            ;
            ld  e,h
            ld  a,3             ; R3: ch.B period MSB
            call WRTPSG
            ;
            ld  a,(t_ampB)
            ld  e,a
            ld  a,9             ; R9: ch.B amplitude
            call WRTPSG
            ;
            jr  bgm_updateC

next_trackB ld  a,(hl)          ; duplicate of next_trackA
            ld  (tracknum),a
            jp  playtrack

bgm_updateC ld  a,10            ; R10: ch.C amplitude
            ld  e,$10           ; variable amplitude (use envelope)
            call WRTPSG
            ;
            ld  a,11            ; R11: envelope period LSB
            ld  e,$00
            call WRTPSG
            ;
            ld  hl,(t_noteidxC)
            ld  a,(hl)
            inc hl
            ld  (t_noteidxC),hl
            cp  $FF
            jr  z,next_trackC
            ;
            cp  $7F             ; = $80-FE ?
            jr  nc,noiseC       ;   yes: it's noise instead of note period index
            ;
            sla a
            ld  c,a
            ld  b,$00
            ld  hl,notetab
            add  hl,bc
            ld  a,(hl)
            ld  e,a
            ld  a,4             ; R4: ch.C period LSB
            call WRTPSG
            ;
            inc hl
            ld  a,(hl)
            ld  e,a
            ld  a,5             ; R5: ch.C period MSB
            call WRTPSG
            ;
            ld  a,7             ; R7: channel enable
            ld  e,%10111000     ; IOB=out IOA=in, noise disabled, tone enabled
            call WRTPSG
            ;
            ld  a,12            ; R12: envelope period MSB
            ld  e,02ch
            call WRTPSG
            ;
            ld  a,(t_envC)
            ld  e,a
            ld  a,13            ; R13: envelope shape/cycle control
            call WRTPSG
            ;
            jp  savedhook       ; done, exit via saved hook

noiseC      ld  b,$7F           ; convert $80-FE range
            sub b               ;   down to $01-7F for env period
            ld  e,a
            ld  a,12            ; R12: envelope period MSB
            call WRTPSG
            ;
            ld  a,7             ; R7: channel enable
            ld  e,%10011100     ; IOB=out IOA=in, C=noise, BA=tone
            call WRTPSG
            ;
            ld  a,(t_envC)
            ld  e,a
            ld  a,13            ; R13: envelope shape/cycle control
            call WRTPSG
            ;
            jp  savedhook       ; continue with original timer interrupt code

next_trackC ld  a,(hl)          ; duplicate of next_trackA
            ld  (tracknum),a
            jp  playtrack


;----------------------------------------------------------------------

; XXX
; BLOCK 'sdata' (start 0x5365 end 0x6a00)   bgm-psgv.asm
; BLOCK 'sdata' (start 0x5365 end 0x57a8)   end-psgv.asm

;----------------------------------------------------------------------
;   Note table: 97 ($61) entries of LSB and MSB of PSG tone channel period

notetab
            dw  $000    ; 4096
            dw  $D5D    ; 3421
            dw  $C9C    ; 3228
            dw  $BE7    ; 3047
            dw  $B3C    ; 2876
            dw  $A9B    ; 2715
            dw  $A02    ; ...
            dw  $973
            dw  $8EB
            dw  $86B
            dw  $7F2
            dw  $780
            dw  $714
            dw  $6AF
            dw  $64E
            dw  $5F4
            dw  $59E
            dw  $54E
            dw  $501
            dw  $4BA
            dw  $476
            dw  $436
            dw  $3F9
            dw  $3C0
            dw  $38A
            dw  $357
            dw  $327
            dw  $2FA
            dw  $2CF
            dw  $2A7
            dw  $281
            dw  $25D
            dw  $23B
            dw  $21B
            dw  $1FD
            dw  $1E0
            dw  $1C5
            dw  $1AC
            dw  $194
            dw  $17D
            dw  $168
            dw  $153
            dw  $140
            dw  $12E
            dw  $11D
            dw  $10D
            dw  $0FE
            dw  $0F0
            dw  $0E3
            dw  $0D6
            dw  $0CA
            dw  $0BE
            dw  $0B4
            dw  $0AA
            dw  $0A0
            dw  $097
            dw  $08F
            dw  $087
            dw  $07F
            dw  $078
            dw  $071
            dw  $06B
            dw  $065
            dw  $05F
            dw  $05A
            dw  $055
            dw  $050
            dw  $04C
            dw  $047
            dw  $043
            dw  $040
            dw  $03C
            dw  $039
            dw  $035
            dw  $032
            dw  $030
            dw  $02D
            dw  $02A
            dw  $028
            dw  $026
            dw  $024
            dw  $022
            dw  $020
            dw  $01E
            dw  $01C
            dw  $01B
            dw  $019
            dw  $018
            dw  $016
            dw  $015
            dw  $014
            dw  $013
            dw  $012
            dw  $011
            dw  $010
            dw  $00F
            dw  $00E

;----------------------------------------------------------------------
;   Current music player state

tracknum    db  $00         ; index of 11-byte entry in `tracktab`

playing     db  $00         ; 0: not playing, original H.TIM hook installed
                            ; 1: playing, our H.TIM hook installed

savedhook   db  5 dup $00   ; A copy of the routine at H.TIMI before we put
                            ; put ours there. Called after we're done to
                            ; continue origina H.TIMI interrupt processing.

wait        db  $00         ; number of ticks to wait until next PSG update

; ----------------------------------------------------------------------
;   Current track state. This is a copy of the track data entry from
;   `tracktab` for the currently playing track.

curtrack

;   Number of 1/60 second ticks to wait after starting each note before
;   moving on to the next note. 15 ($0F) = eighth notes at 120 bpm.
t_tempo     db  $00

;   Note data. Each byte is one of the following:
;   - $FF: End of track indicator. This may appear in any of the three
;     channels; the first one encountered will terminate playback of this
;     track. The following byte is the `tracktab` index of the track to
;     play next, or $FF to stop playback.
;   - $00-60 (0-96): An word index into `notetab` indicating what note to
;     play. Each word in `notetab` is the 8-bit LSB and 4-bit MSB of the
;     period for that channel: 4096, 3421, 3228, ..., 14.
;   - $80-FE (ch.C only): Use noise instead of tone for this note. The
;     value - $7F is the MSB of the period for the envelope (higher values
;     are shorter noise bursts).
t_noteidxA  dw  $0000
t_noteidxB  dw  $0000
t_noteidxC  dw  $0000

;   Channel amplitude.
;   - Ch.A and B are in the standard amplitude register format `%xxxENNNN`
;     where E determines if the volume is fixed (0) or uses the envelope
;     generator (1), and NNNN is the volume (0-15) to use if it's fixed.
;   - Ch.C always uses the envelope generator and the value sets the
;     envelope waveform using the standard envelope shape/cycle control
;     register format. $00 indicates a decay from full volume to 0 over
;     the envelope period, followed by the note/noise staying off.
t_ampA      db  $00         ; ch.A amplitude (R8, xxxMnnnn) (lead voice?)
t_ampB      db  $00         ; ch.B amplitude (R9)
t_envC      db  $00         ; envelope shape/cycle (R13, used only on ch.C)

;   Channel B detune. The value is added to the `notetab` period determined
;   for ch.B. which will make the note slightly flatter. This is apparently
;   somehow useful for bass lines.
flatB       db  $00         ; add to period for ch.B to detune it flat
