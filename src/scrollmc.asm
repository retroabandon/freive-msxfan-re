;   Slime World: scrollmc
;
;   Copy data in VRAM data from `vram_src` to (constant) VRAM_DEST`.
;
;   Assemble with: z80asm -l -i src/scrollmc.asm -o tmp/scrollmc.bin
;   (This produces a raw binary, not a BLOADable file.)
;
;   This code is relocatable, though the storage it uses is not. Aside from
;   the vram_src parameter word, it requires 34 bytes of temporary storage
;   (bdest, bsrc, buf) which are all currently immediately after the `ret`.
;
;   vram_src set by BASIC (SLIMMAIN.SLE pokes $DD41/2 at lines 1015, 1040,
;   4510, 4700). It's not clear if this is always set before a USR(),
;   however; line 700 does not POKE a value first. If not, it may need to
;   be preserved between calls.
;

            include asm-setup.inc

            org     $dd00

;----------------------------------------------------------------------
;   Definitions

;   The map area on the screen has a border around it, taking up all of
;   rows 0 and 15 and columns 0 and 31 between those rows. To fill the
;   area inside we copy 14 rows of 30 columns each.
screen_rows:    equ 14
screen_cols:    equ 30
screen_base:    equ $1800                   ; row 0, column 0
vram_dest:      equ screen_base + 32 + 1    ; 1 row + 1 column

map_rowlen:     equ $80         ; rows in source map are 128 bytes long
screen_rowlen:  equ 32

;   Block xfer between RAM and VRAM.
;   BC=block length, HL=source start addr, DE=dest start addr.
LDIRMV:         equ $059        ; memory ← VRAM
LDIRVM:         equ $05c        ;   VRAM ← memory

;----------------------------------------------------------------------
;   Copy routine

init:       ;   This sets the entrypoint to match the original binary,
            ;   though it isn't actually used by the BASIC code.

scrollmc:   ld hl,(vram_src)    ; VRAM source addr supplied by caller
            ld de,vram_dest     ; fixed VRAM target address
            ld (bdest),de       ; current block addrs set to
            ld (bsrc),hl        ;   src and dest start addrs
            ld b,screen_rows    ; loop count

nextb:      push bc             ; save loop count

            ld de,buf           ; dest addr in main RAM
            ld hl,(bsrc)        ; source in VRAM
            ld bc,buflen        ; one buffer's worth
            push hl
            push de
            call LDIRMV         ; copy main RAM=buf ← VRAM=[bsrc]
            pop de
            pop hl

            ld bc,map_rowlen    ; bsrc moves up to next row of map
            add hl,bc
            ld (bsrc),hl

            ex de,hl            ; hl=buf: swap source/target for copy back
            ld de,(bdest)       ; VRAM target addr for this block
            ld bc,buflen
            push hl
            push de
            call LDIRVM         ; copy VRAM=(bdest) ← mem=buf
            pop hl
            pop de

            ld bc,screen_rowlen ; bdest moves up to next screen row
            add hl,bc
            ld (bdest),hl

            pop bc              ; restore loop count
            djnz nextb          ; decrement b and loop unless 0
            ret

;----------------------------------------------------------------------
;   Parameter and temporary storage

;   Unfortunately z80asm still writes data (default 0) to the ouput file
;   for space that's merely reserved. We don't worry about this since
;   a few extra bytes in the final BLOAD data will do no harm and in fact
;   help indicate that we use some memory just after the code.
;
vram_src    dw ?                ; VRAM source starting address
bdest       dw ?                ; current block VRAM destination address
bsrc        dw ?                ; current block VRAM source address
buflen      equ screen_cols
buf         db buflen dup ?
