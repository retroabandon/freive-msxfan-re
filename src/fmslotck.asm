;
;   fmslotck.asm - Detect and set up FM-BIOS
;
;   WARNING: This program has some unusual bits of memory behaviour that
;   mean you need to be careful where you load it to ensure that it's
;   not stomping on other things, and that the FM player code and data
;   (and any other programs) do not stomp on what this has set up.
;

            include asm-setup.inc
            include msx.inc

MB          function addr,((addr & $FF00) >> 8) ; return MSB of address

; ----------------------------------------------------------------------

page1       equ $4000       ; Start address of MSX page 1.
fmbioslen   equ $0FFF       ; Length of FM-BIOS code to copy.
                            ;   (This copies 4 KB - 1 byte, though the actual
                            ;   length is only 2.75 KB for the BIOS code
                            ;   and another 0.5 KB for the voice data.)

romsig      equ $401C       ; Start of FM-BIOS ROM signature:
                            ;   will be "OPLL" if FM-BIOS present in that page.

;   The FM-BIOS needs a 160 byte work area in RAM.
;   XXX not clear how we know that this area is ok to use.
fmworkarea  equ $DD50

;   $DE3C-$DE3E are used by the BASIC programs to store/check "PSG" or "FM ".
;               In BASIC, -8644-1 or -8643 through -8641.

fmslotdesc  equ  $DE3F      ; Slot desc of FM-BIOS ROM, or 0 if not present.
                            ;   Used only while this program is running.

; ----------------------------------------------------------------------

            org  $C800

init:       ;   This sets the entrypoint to match the original binary,
            ;   though the entrypoint isn't actually used by the BASIC code.

usr         call searchslots
            ld   a,(fmslotdesc) ; slotdesc of FM-BIOS, or 0 if not found
            ld   (usrarg_int),a ; assume USR(nn%) (integer argument)
            or   a              ; FM-BIOS found? (A ≠ 0?)
            jr   z,.return      ;    no: init neither needed nor possible

            ;   Copy FM-BIOS to temporary area in high memory.
            ld   h,MB(page1)    ; map FM-BIOS ROM to page 1
            call ENASLT         ; A contains slotdesc from above
            ld   hl,page1
            ld   de,temp
            ld   bc,fmbioslen
            ldir
            ;   Copy FM-BIOS from temp area to page 1 RAM.
            ld   a,(RAMAD1)     ; page 1 RAM slot descriptor
            push af             ; save a copy
            ld   h,MB(page1)
            call ENASLT
            ld   hl,temp
            ld   de,page1
            ld   bc,fmbioslen
            ldir
            ;   Initialise FM-BIOS.
            pop  iy             ; pop page 1 RAM slotdesc (LSB ignored)
            ld   ix,INIOPL      ; initialize OPLL
            ld   hl,fmworkarea  ; 160 bytes in RAM for FM-BIOS use
            call CALSLT
            ;
.return     ld   a,(EXPTBL)     ; restore main BIOS ROM (BASIC)
            ld   h,MB(page1)    ;   to page 1
            call ENASLT
            ret

searchslots ld   b,4            ; loop 4 → 1
.outer      push bc             ; save outer loop counter
            ld   a,4
            sub  b              ; primary slot numbers 0 → 3
            ld   c,a            ; current slotdesc ← primary only %000000pp
            ld   hl,EXPTBL      ; base of slotdesc table for primary slots
            ld   e,a            ; index of current primary slot
            ld   d,0
            add  hl,de          ; calculate offset of cur. primary slot
            ld   a,(hl)         ;    and load its slotdesc
            add  a,a            ; is it >$7F? (b7 set? has expansion slots?)
            jr   nc,.notexp     ;   no: check primary slot ony
            ld   b,4
.expslot    push bc             ; save inner loop counter
            ld   a,$24
            sub  b              ; $20-$23: %001000ee
            rlca                ; shift left to build expanded slot number
            rlca                ;   and expanded bit: %1000ee00
            or   c              ; or in pri slot number: %1000eepp
            call checksig
            pop  bc             ; restore inner loop counter
            jr   z,.done        ; if found, exp slotdesc is in `fmslotdesc`
            djnz .expslot
.notfound   xor  a              ; checksig left slotdesc it checked in
            ld   (fmslotdesc),a ;   `fmslotdesc`; replace w/0 to indicate
                                ;   FM-BIOS not found
            pop  bc             ; restore outer loop counter
            djnz .outer         ; next loop, if not done checking all pri slots
            ret                 ; return FM-BIOS not found (fmslotdesc=0)
            ;
.notexp     ld   a,c            ; not an expanded slot; slotdesc is just
            call checksig       ;    current primary: %000000pp
            jr   nz,.notfound   ; if failed, then return not found
            ;   Otherwise checkdesc left slotdesc it checked in `fmslotdesc`.
.done       pop  bc             ; pop outer loop counter
            ret                 ; return FM-BIOS found (fmslotdesc=slotdesc)

;   This is the signature we expect to see at `romsig`
;   if that page contains an MSX-MUSIC FM-BIOS.
fmsig       db  "OPLL"
fmsiglen    equ $ - fmsig

;   ♠AF ♡BC ♣DE,HL Given a slot descriptor in A
;   check to see if it has an FM-BIOS signature `romsig`.
;   Returns A=0 on success, A=1 on failure.
checksig    ld   (fmslotdesc),a
            push bc             ; preserve registers
            ld   hl,romsig
            ld   de,fmsig
            ld   b, fmsiglen
.next       push af             ; save slotdesc
            push bc             ; BC, DE are destroyed by RDSLT
            push de
            call RDSLT
            ei                  ; re-enable interrupts left disabled by RDSLT
            pop  de
            pop  bc             ; destroyed regs restored
            ld   c,a            ; byte returned from RDSLT
            ld   a,(de)         ; byte of `fmsig`
            cp   c              ; same?
            jr   nz,.nomatch    ;    no: return failure to match
            pop  af             ; restore slotdesc
            inc  de             ; next bytes
            inc  hl             ;   to compare
            djnz .next
.match      pop  bc             ; restore preserved regs
            xor  a              ; return 0 to indicate success
            ret
            ;
.nomatch    pop  af             ; cleanup loop data
            pop  bc             ; restore preserved regs
            xor  a
            inc  a              ; return 1 to indicate failure to match
            ret

temp        ds   fmbioslen
