;
;   end-psgv.asm - top level build of music code
;
;   This rebuilds, as closely as possible, the original binary file
;   END-PSGV.SLM for the game's music.
;
;   The original disassembly was done with tool/slm-extract using the
;   following parameters, though this has been extensively changed from
;   that.
;
; msx-dasm load=$C1AF end=$C77C entry=$C1AF
; z80dasm 1.1.5
; command line: z80dasm -l --origin 0xc1af --sym-input info/END-PSGV.SLM.sym --block-def info/END-PSGV.SLM.block /tmp/tmpebj_wibn/obj
;

            include asm-setup.inc
            include msx.inc

            org $C1AF           ; load address before relocations

;   XXX The actual length of the data in the original file, as calculated
;   by sdata_end - p1dst, is $5A8. However, the original code uses a length
;   of $1800, probably because the code is a direct copy of bgm-psgv.slm.
;   Thus this copies extra of random data, but that causes the player no
;   harm.
;
;   We maintain the old length for the moment to make binary comparisons
;   easier. When we start using the proper length, we'll want to use a
;   standalone player (like that in SLIMOPNG.SLM) to test that the last
;   track plays all the way to the end.
;
p1dst_len   equ $1800           ; sdata_end - p1dst = $5A8

hicalls     equ $DE00           ; high-memory code to call page1 code

            include bgm-init.asm        ; relocation routines at load point
bgm_usr_src include bgm-hi.asm          ; relocated to high memory
p1dst_src   include psgv.asm            ; relocated to page 1

            include end-psgv-tracks.asm ; music data

;   Two bytes from the original that are probably garbage left over from
;   original development. We keep these so that we are as closely as
;   possible reconstructing the original binary.
            db  $5C, $00

sdata_end
