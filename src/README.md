Slime World Sources
===================

These have been reverse-engineered from the object code.

### BASIC Code

    slimesta.ba
    slimopng.slm
    slimmain.slm
    slimendp.slm

BASIC code expanded with `bin/detok -e` and with some reverse-engineering
(commenting and explaining the code). These files are currently for
reference only; they cannot yet be turned back into files that can be
loaded on an MSX machine.

### Assembly Code Generic Definitions

- `asm-setup.inc`: Setup for the assembler ([Macroassembler AS]) itself.
  Enables use of all number formats so we can use `$AB` instead of `0abh`.

- `msx.inc`: MSX system definitions, incuding system calls, data locations,
  and so on.

### Reverse-engineered Assembly Code Subroutines

These are disassembled and reverse-engineered versions of common routines
extracted from the `{bgm,end}-{fmvs,psgv}.asm` files below. The routines
have been removed from those files and brought back in via `include` of the
files below.

- `bgm-init.asm`: The routine run just after load that copies the music
  player and its data to page 1 and the USR3 routine to its call location
  in higher memory.

- `bgm-hi.asm`: The USR3 routine and H.TIMI timer hook. Both are in high
  (BASIC-accessible) RAM; they bank RAM into page 1 and call routines there.

- `psgv.asm`: PSG music player.

- `fmvs.asm`: FM music player.

### Disassemblies and RE

All reverse-engineered versions of the original binary files generate
almost exact equivalents of the originals. The original files are padded
with $1A out to a 128-byte boundary (probably a quirk of how the files were
copied from the MSX disk), so `Test srcdiff` also pads out to that boundary
before doing the comparison to avoid a spurious difference.

- `fmslotck.asm`: Detects the presence of an FM sound generator.
- `scrollmc.asm`: Updates the screen to scroll the map by copying VRAM areas.

- `bgm-psgv.asm`: Intro and main game music, PSG version.
- `end-psgv.asm`: Ending sequence music, PSG version.
- `bgm-fmvs.asm`: Intro and main game music, FM version.
- `end-fmvs.asm`: Ending sequence music, FM version.

These build the music player, music data and init code for parts/versions
of the game. They are top-level files that contain just basic definitions
for that particular assembly and `include` statements to bring in the
actual player code (above) and music data (below).

- `bgm-psgv-tracks.asm`: Music data for intro and main game, PSG version.
- `end-psgv-tracks.asm`: Music data for ending sequence, PSG version.



<!-------------------------------------------------------------------->
[Macroassembler AS]: http://john.ccac.rwth-aachen.de:8000/as/as_EN.html#sect_3_9_7_
