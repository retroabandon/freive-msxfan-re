;
;   end-psgv.asm - top level build of music code
;
;   This rebuilds, as closely as possible, the original binary file
;   END-PSGV.SLM for the game's music.
;
;   The original disassembly was done with tool/slm-extract using the
;   following parameters, though this has been extensively changed from
;   that.
;
; msx-dasm load=$C1AF end=$D9B1 entry=$C1AF
; z80dasm 1.1.5
; command line: z80dasm -l --origin 0xc1af --sym-input info/BGM-PSGV.SLM.sym --block-def info/BGM-PSGV.SLM.block /tmp/tmpyoh_y7m1/obj

            include asm-setup.inc
            include msx.inc

            org $C1AF           ; load address before relocations

p1dst_len   equ sdata_end - p1dst
hicalls     equ $DE00           ; high-memory code to call page1 code

            include bgm-init.asm        ; relocation routines at load point
bgm_usr_src include bgm-hi.asm          ; relocated to high memory
p1dst_src   include psgv.asm            ; relocated to page 1

            include bgm-psgv-tracks.asm ; music data

            db $4E dup ?        ; to get same p1dst_len as original

sdata_end
