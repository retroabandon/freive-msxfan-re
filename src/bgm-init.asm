;
;   bgm-init.asm - relocates Slime World music player low and high code
;

init        di              ; interrupts must be disabled when ROM switched out

            ;   After reset the MSX2 system ROM (or disk ROM?) will find
            ;   two pages of RAM to assign to page 0 ($0000) and 1 ($4000)
            ;   address space and store their slot descriptors (E000SSPP)
            ;   in RAMAD0 and RAMAD1. These are unused by BASIC so we can
            ;   store our music routines and data in the page 1 RAM so long
            ;   as we ensure we don't hand control to the BASIC interpreter
            ;   while it's mapped in.
            ld a,(RAMAD1)       ; slot descriptor for default page 1 RAM
            ld h,040h           ; page 1 start addr hi
            ld l,000h           ;   and lo
            call ENASLT         ; map page

            ;   Copy the music player routine and its data from the
            ;   original BLOAD point into the page 1 RAM.
            ld hl,p1dst_src     ; BLOADed data start address
            ld de,p1dst         ; destination address in page 1
            ld bc,p1dst_len     ; length
            ldir                ; copy

            ;   The routine to request playing a particular BGM track must
            ;   be in in BASIC RAM (pages 3 and 4) so it's available while
            ;   the BASIC interpreter is running and page 1 is mapped to
            ;   BASIC ROM. Copy it from the load location to where it lives
            ;   whie the program is running. If it's in the BASIC text
            ;   area, subsequent BASIC programs must immediately reserve
            ;   this memory using the CLEAR statement to avoid BASIC
            ;   stomping on this.
            ld hl,bgm_usr_src   ; BLOADed data start address
            ld de,bgm_usr       ; destination address
            ld bc,bgm_hi_end - bgm_usr  ; length
            ldir                ; copy

            ;   Restore page 1 mapping to BASIC ROM and re-enable
            ;   interrupts before returning to BASIC interpreter.
            ld a,(EXPTBL)       ; slot descriptor for page 1 main BIOS/BASIC ROM
            ld h,040h           ; page 1 start addr hi
            ld l,000h           ;   and lo
            call ENASLT         ; map page
            ei
            ret
