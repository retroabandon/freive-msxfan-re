;
;   bgm-usr3.asm - Slime World music player high code
;
;   This code needs to live in page 3 or 4 so that it can be called from
;   the BASIC interpreter.
;
            phase hicalls

;   USR() call to control BGM player
bgm_usr     di
            push af             ; preserve AF
            ld a,(RAMAD1)       ; load slot descriptor for RAM in page 1
            push af             ; copy to IY(hi) for CALSLT
            pop iy
            ld ix,p1dst         ; call addr
            call CALSLT         ; call IX after bringing in page IY
            pop af              ; restore original AF
            ei
            ret

;   H.TIMI call to operate BGM player (60 times/second)
bgm_tick    di
            push af
            ld a,(RAMAD1)
            push af
            pop iy
            ld ix,bgm_update    ; different call addr; all else as `bgm_usr_src`
            call CALSLT
            pop af
            ei
            ret

bgm_hi_end  dephase
