;
;   fmvs.asm - FM player code from Slime World
;
;   This is the common player code (and apparenty some common data) for the
;   game and end sequence. The sound data are expected to start at the
;   `tracktab` symbol, usually appended to this code.
;
;   WARNING: This code calls the FM-BIOS, but it does NOT map page 1
;   to the FM-BIOS ROM when doing so; instead it assumes that the FM-BIOS
;   has been copied to the page 1 RAM that this code is running from.
;   (That's set up by the `fmslotck.asm` program.) Thus, you need to make
;   sure that the start address of this is above the 2.75 KB for FM-BIOS
;   code and additional 0.5 KB for voice data, i.e., $4D00 or higher.)
;

; msx-dasm load=$4E00 end=$7FFE entry=$0000
; z80dasm 1.1.6
; command line: z80dasm -l --origin 0x4e00 --sym-input info/z.sym --block-def info/z.block /tmp/tmp9ytk1ulf/obj

            phase $4E00

p1dst       ld   a,(trackcnt)
            ld   b,a
            ld   a,(usrarg_int) ; assume USR(nn%) (integer argument)
            cp   $FE            ; tracknum = 254 ?
            jp   z,retplaying   ;   yes: return playing status
            jp   nc,stopbgm     ; tracknum > 254: stop music
            cp   b              ; tracknum > number of tracks available?
            jp   nc,stopbgm     ;   yes: stop music

            ;   Set up timer hook to call player routine
            call stopbgm
            di
            ld   hl,H.TIMI      ; save previous timer hook
            ld   de,savedhook
            ld   bc,5
            ldir
            ld   a,$C3          ;  `jp nnnn` instruction
            ld   (H.TIMI),a
            ld   hl,bgm_tick
            ld   (H.TIMI+1),hl

            ;   Configure vendor-specified values for rhythm mode. This
            ;   lets player enable rhythm mode without further configuration,
            ;   or it can overwrite these if it uses 9-voice tone mode.
            ld   a,OPLL_rfnum
            ld   e,OPLL_rfnval0
            call WRTOPL
            inc  a              ; OPLL_rfnum+1
            ld   e,OPLL_rfnval1
            call WRTOPL
            inc  a              ; OPLL_rfnum+2
            ld   e,OPLL_rfnval2
            call WRTOPL
            ld   a,OPLL_rconf
            ld   e,OPLL_rcval0
            call WRTOPL
            inc  a              ; OPLL_rconf+1
            call WRTOPL         ; re-use OPLL_rcval0, it = OPLL_rcval1
            inc  a              ; OPLL_rconf+2
            ld   e,OPLL_rcval2
            call WRTOPL

            ;   Find the data for the track we've been requested to play.
            ld   a,(usrarg_int)
            add  a,a            ; A ← A * 16
            add  a,a
            add  a,a
            add  a,a
            ld   c,a            ; offset LSB
            ld   b,0            ; offset MSB always $00 (max 15 tracks)
            ld   hl,tracktab
            add  hl,bc          ; HL ← track data (16 bytes) address
            ld   b,8
            ;   Init voice0[0:2] and voice0[2:4] with the first word
            ;   of tracktab, then voice0[4:20] with voice_init.
            ;   Repeat for all 8 voices.
            ld   de,voice0
.loop8x     push bc             ; save loop counter
            ld   a,(hl)
            ld   (de),a         ; voice0[0] (LSB) ← track[0]
            inc  de
            inc  de
            ld   (de),a         ; voice0[2] (LSB) ← track[0]
            inc  hl
            dec  de
            ld   a,(hl)
            ld   (de),a         ; voice0[1] (MSB) ← track[1]
            inc  de
            inc  de
            ld   (de),a         ; voice0[3] (MSB) ← track[1]
            inc  de             ; voice0 = voice0 + 4
            inc  hl             ; track    = track    + 2
            ;
            push hl             ; save src[2] pointer
            ld   hl,voice_init
            ld   bc,16          ; voice0[4:20] ← voice_init[0:16]
            ldir                ; DE ← DE + 16, bringing it to next voice
            pop  hl
            ;
            pop  bc             ; restore loop counter
            djnz .loop8x        ; decrement B and loop if still non-zero

            ld   a,%01111111    ; PSG noise (b6) and 6 FM tone voices active
            ld   (vactive),a

            xor  a
            ld   (psgCamp),a    ; PSG ch.C noise will be off
            call CHGSND         ; 1-bit sound value init to 0 for `click`

            ld   a,1            ; playing ← true
            ld   (playing),a
            ret

stopbgm     ld   a,(playing)    ; 1 if we're playing
            or   a              ; set flags
            ret  z              ; if not playing, we're done
            ;
            xor  a              ; A=0
            ld   (playing),a
            ld   hl,savedhook   ; restore previous timer routine
            ld   de,H.TIMI
            ld   bc,5
            ldir
            ;
            ld   a,OPLL_vinst   ; voice instrument/volume base register
            ld   e,$FF          ; elec. guitar/bass, full volume
            ld   b,9            ; iterate over 9 voices
-           call WRTOPL
            inc  a              ; next voice
            djnz -
            ;
            ld   a,OPLL_vconf   ; voice config regs
            ld   e,$00          ; key off
            ld   b,9            ; iterate over 9 voices
-           call WRTOPL
            inc  a              ; next voice
            djnz -
            ;
            ld   a,OPLL_rhythm
            ld   e,%100000      ; enable rhythm mode, all drums off
            call WRTOPL
            call GICINI         ; silence PSG
            ret

            ;   Return to USR() 1 if we're playing, 0 if we're stopped.
retplaying  ld   a,(playing)
            ld  (usrarg_int),a
            ret

bgm_update  ld   b,6            ; loop through 6 FM tone voices
            ld   a,(vactive)
            ld   hl,curvoice    ; start updating
            ld   (hl),0         ;   from voice 0
.loop6      push bc             ; save loop counter
            rrca                ; carry ← next voice active bit
            push af             ; save remaining voice active bits
            push hl             ; save current voice number
            call c,_4EEH
            pop  hl
            inc  (hl)           ; increment current voice number
            pop  af             ; restore remaining voice active bits
            pop  bc             ; restore loop counter
            djnz .loop6
            rrca                ; carry ← PSG noise active bit
            call c,rhythm_upd   ; if rhythm active, update drums, noise, click
            jp   savedhook      ; continue with original timer interrupt code

_4EEH       ld   a,(curvoice)
            add  a,a            ;     A *  2
            ld   b,a            ; B ← A *  2
            add  a,a            ;     A *  4
            add  a,a            ;     A *  8
            add  a,b            ;     A * 10
            add  a,a            ;     A * 20
            ld   c,a            ; C ← original A * 20
            ld   b,0
            ld   ix,voice0
            add  ix,bc          ; IX ← voice0 + original A * 20 for voice offset
            ld   a,(ix+9)
            or   a
            jr   nz,.F420
            ld   a,(ix+7)
            dec  a
            ld   (ix+7),a
            jr   nz,.F420
            ld   a,(ix+15)
            and  %00101111      ; b4 key on/off ← 0 to mute voice
            ld   (ix+15),a
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vconf
            call WRTOPL
.F420       ld   a,(ix+6)
            dec  a
            ld   (ix+6),a
            jp   nz,l4f80h
            ;
            ld   a,(ix+9)
            or   a
            jr   nz,l4f3eh
            ld   a,(ix+15)
            and  $2F
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vconf
            call WRTOPL
            ;
l4f3eh      ld   l,(ix+4)
            ld   h,(ix+5)
l4f44h      ld   a,(hl)
            cp   060h
            jp   c,l4fa6h
            cp   070h
            jp   c,l5000h
            cp   080h
            jp   c,l5015h
            cp   0ffh
            jp   z,l50dah
            sub  080h
            call sub_4f74h
            ld   l,$50
            dec  (hl)
            ld   d,b
            dec  a
            ld   d,b
            ld   h,(hl)
            ld   d,b
            sub  b
            ld   d,b
            sub  a
            ld   d,b
            sbc  a,a
            ld   d,b
            and  a
            ld   d,b
            xor  a
            ld   d,b
            cp   a
            ld   d,b
            bit  2,b
sub_4f74h   ex   (sp),hl        ; get caller's address
            add  a,a            ; increment it by A words
            ld   d,0
            ld   e,a
            add  hl,de
            ld   e,(hl)         ; DE ← value it points to
            inc  hl
            ld   d,(hl)
            ex   de,hl          ; leave old top-of-stack value in DE
            ex   (sp),hl        ; new value at top of stack
            ret

l4f80h      ld   e,(ix+12)
            ld   d,(ix+13)
            ld   a,e
            or   d
            ret  z
            ;
            ld   l,(ix+14)
            ld   h,(ix+15)
            add  hl,de
            ld   (ix+14),l
            ld   (ix+15),h
            ld   e,l
            ld   a,(curvoice)
            add  a,OPLL_vfnum
            call WRTOPL
            ld   e,h
            add  a,OPLL_vfnum
            call WRTOPL
            ret

l4fa6h      inc  hl
            push hl
            inc  hl
            ld   (ix+4),l
            ld   (ix+5),h
            ld   b,(ix+18)
            add  a,b            ; index
            add  a,a            ; word offset
            ld   c,a
            ld   b,0
            ld   hl,unk_data
            add  hl,bc
            ld   a,(hl)
            ld   b,(ix+11)
            add  a,b
            ld   (ix+14),a
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vfnum
            call WRTOPL
            inc  hl
            ld   a,(hl)
            ld   b,(ix+10)
            and  01fh
            or   b
            ld   (ix+15),a
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vconf
            call WRTOPL
            pop  hl
            ld   a,(hl)
            ld   (ix+6),a
            ld   l,a
            ld   h,0
            ld   b,(ix+8)       ; number of times to double HL
.loop       djnz .double        ; if B, continue doubling until done
            srl  h
            rr   l              ; HL/2
            srl  h
            rr   l              ; HL/4
            srl  h
            rr   l              ; HL/8
            ld   (ix+7),l
            ret
            ;
.double     add  hl,hl
            jr   .loop

l5000h      sub  060h
            ld   (ix+16),a
            ld   b,(ix+17)
            or   b
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vinst
            call WRTOPL
            jp   l50d0h

l5015h      sub  070h
            add  a,a
            add  a,a
            add  a,a
            add  a,a
            ld   (ix+17),a
            ld   b,(ix+16)
            or   b
            ld   e,a
            ld   a,(curvoice)
            add  a,OPLL_vinst
            call WRTOPL
            jp   l50d0h

            xor  a
            ld   (ix+10),a
            jp   l50d0h

            ld   a,020h
            ld   (ix+10),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            push hl
            ld   hl,instbuf
            call RDDATA             ; read instrument
            ld   b,8
            xor  a
            ld   (ix+17),a
            ld   hl,instbuf
-           ld   e,(hl)
            call WRTOPL
            inc  hl
            inc  a
            djnz -
            ld   e,(ix+16)
            ld   a,(curvoice)
            add  a,OPLL_vinst
            call WRTOPL
            pop  hl
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            push hl
            add  a,a
            add  a,a
            add  a,a
            ld   c,a
            ld   b,0
            ld   hl,sdataA
            add  hl,bc
            ld   b,8
            xor  a
            ld   (ix+17),a
-           ld   e,(hl)
            call WRTOPL
            inc  hl
            inc  a
            djnz -
            ld   e,(ix+16)
            ld   a,(curvoice)
            add  a,OPLL_vinst
            call WRTOPL
            pop  hl
            jp   l50d0h

            xor  a
            ld   (ix+9),a
            jp   l50d0h

            ld   a,1
            ld   (ix+9),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            ld   (ix+8),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            ld   (ix+11),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            xor  $FF
            inc  a
            ld   (ix+12),a
            ld   a,$FF
            ld   (ix+13),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            ld   (ix+12),a
            xor  a
            ld   (ix+13),a
            jp   l50d0h

            inc  hl
            ld   a,(hl)
            ld   (ix+18),a
            ; fallthrough to l50d0h

l50d0h      inc  hl
            ld   (ix+4),l
            ld   (ix+5),h
            jp   l4f44h

l50dah      ld   a,(ix+19)
            dec  a
            ld   (ix+19),a
            jr   z,l50f6h
            ld   l,(ix+2)
            ld   h,(ix+3)
            dec  hl
            ld   a,(hl)
            ld   (ix+5),a
            dec  hl
            ld   a,(hl)
            ld   (ix+4),a
            jp   l4f3eh
            ;
l50f6h      ld   l,(ix+2)
            ld   h,(ix+3)
            ld   a,(hl)
            or   a
            jr   z,l5117h
            ld   (ix+19),a
            inc  hl
            ld   a,(hl)
            ld   (ix+4),a
            inc  hl
            ld   a,(hl)
            ld   (ix+5),a
            inc  hl
            ld   (ix+2),l
            ld   (ix+3),h
            jp   l4f3eh
            ;
l5117h      inc  hl
            ld   a,(hl)
            cp   0ffh
            jr   z,l5142h
            ld   b,a
            add  a,a
            add  a,b
            ld   c,a
            ld   b,0
            ld   l,(ix+0)
            ld   h,(ix+1)
            add  hl,bc
            ld   a,(hl)
            ld   (ix+19),a
            inc  hl
            ld   a,(hl)
            ld   (ix+4),a
            inc  hl
            ld   a,(hl)
            ld   (ix+5),a
            inc  hl
            ld   (ix+2),l
            ld   (ix+3),h
            jp   l4f3eh
            ;
l5142h      ld   a,(curvoice)
            inc  a              ; .loop below: current FM tone voice to first
            ld   b,a
            ld   e,0            ; voice off
            add  a,OPLL_vconf-1 ; compensate for inc a above
            call WRTOPL
            ld   e,$FF
            add  a,OPLL_vfnum
            call WRTOPL
            ;
            xor  a              ; A = 0
            scf                 ; set carry
.loop       rl   a              ; rotate carry into bit 0 of A
            djnz .loop          ;    and repeat for each voice up to curvoice
            xor  $FF            ; invert
            ld   b,a
            ld   a,(vactive)
            and  b              ; disable all voices from curvoice to first
            ld   (vactive),a
            or   a              ; are all voices off?
            jp   z,stopbgm      ;   yes: disable BGM H.TIMI hook and return
            ret

rhythm_upd  ld   ix,voice6
            call psgnoise       ; preserves IX
            ld   a,(ix+6)
            dec  a
            ld   (ix+6),a
            ret  nz
rhythm_upd2 ld   l,(ix+4)       ; voice_init sets to _53E8
            ld   h,(ix+5)
            ld   a,(hl)
            cp   $FF            ; initial value at _53E8
            jp   z,l52a6h
            bit  7,a
            jp   nz,l5218h
            ld   d,a
            ld   e,020h
            ld   a,00eh
            call WRTOPL
            ld   e,d
            call WRTOPL
            ld   a,d
            rlca
            rlca
            rlca
            rlca
            call c,click
            rlca
            call c,sub_51dch
            rlca
            rlca
            call c,sub_51b4h
            inc  hl
            ld   a,(hl)
            ld   (ix+6),a
            inc  hl
            ld   (ix+4),l
            ld   (ix+5),h
            ret

sub_51b4h   push af
            ld   a,6            ; PSG noise period
            ld   e,2            ; period 2/15
            call WRTPSG
            inc  a              ; (7) PSG channel enable register
            ld   e,%10011100    ; ch.C noise, ch.B,A tone
            call WRTPSG
            ;
            ld   a,5            ; PSG noise ampl. will step down every 5 ticks
            ld   (psgCnextstep),a
            ld   (psgCampstep),a
            ;
            ld   a,(voice6+17)
            xor  $FF
            and  $0F
            ld   (psgCamp),a
            ld   e,a
            ld   a,PSG_C_AMP
            call WRTPSG
            pop  af
            ret

sub_51dch   push af
            ld   a,6
            ld   e,$11
            call WRTPSG
            inc  a              ; A ← 7
            ld   e,$9C
            call WRTPSG
            ld   a,3            ; PSG noise ampl. will step down every 3 ticks
            ld   (psgCnextstep),a
            ld   (psgCampstep),a
            ld   a,(voice6+16)
            xor  0ffh
            and  00fh
            add  a,007h
            srl  a
            ld   (psgCamp),a
            ld   e,a
            ld   a,PSG_C_AMP
            call WRTPSG
            pop  af
            ret

;   Produce a "click" using the 1-bit sound port.
click       push af
            ld   a,1            ; 1-bit sound toggle 0 → 1
            call CHGSND
            ld   b,18
.loop       djnz .loop          ; short delay
            xor  a
            call CHGSND         ; 1-bit sound toggle 1 → 0
            pop  af
            ret

l5218h      ld   b,a
            inc  hl
            ld   c,(hl)
            inc  hl
            ld   (ix+4),l
            ld   (ix+5),h
            rrc  b
            call c,sub_524dh
            rrc  b
            call c,sub_525dh
            rrc  b
            call c,sub_5267h
            rrc  b
            call c,sub_5277h
            rrc  b
            call c,sub_5281h
            ld   a,037h
            ld   e,(ix+16)
            call WRTOPL
            inc  a
            ld   e,(ix+17)
            call WRTOPL
            jp   rhythm_upd2

sub_524dh   ld   a,c
            add  a,a
            add  a,a
            add  a,a
            add  a,a
            ld   d,a
            ld   a,(ix+16)
            and  00fh
            or   d
            ld   (ix+16),a
            ret

sub_525dh   ld   a,(ix+17)
            and  0f0h
            or   c
            ld   (ix+17),a
            ret

sub_5267h   ld   a,c
            add  a,a
            add  a,a
            add  a,a
            add  a,a
            ld   d,a
            ld   a,(ix+17)
            and  00fh
            or   d
            ld   (ix+17),a
            ret

sub_5277h   ld   a,(ix+16)
            and  0f0h
            or   c
            ld   (ix+16),a
            ret

sub_5281h   ld   a,036h
            ld   e,c
            call WRTOPL
            ret

psgnoise    ld   a,(psgCnextstep) ; ticks unti we step down amplitude
            dec  a                ; now one less
            ld   (psgCnextstep),a
            ret  nz              ; if not 0, continue at this amplitude
            ld   a,(psgCampstep) ; otherwise reset and step down
            ld   (psgCnextstep),a
            ld   a,(psgCamp)
            or   a              ; saved PSG C amplitude zero?
            ret  z              ;   yes: no more noise; return
            dec  a              ; no: decrement it,
            ld   (psgCamp),a    ;   save it for next time,
            ld   e,a
            ld   a,PSG_C_AMP    ;   and update PSG
            call WRTPSG
            ret

l52a6h      ld    a,(ix+19)
            dec   a
            ld    (ix+19),a
            jr    z,l52c2h
            ld    l,(ix+2)
            ld    h,(ix+3)
            dec   hl
            ld    a,(hl)
            ld    (ix+5),a
            dec   hl
            ld    a,(hl)
            ld    (ix+4),a
            jp    rhythm_upd2

l52c2h      ld   l,(ix+2)
            ld   h,(ix+3)
            ld   a,(hl)
            or   a
            jr   z,l52e3h
            ld   (ix+19),a
            inc  hl
            ld   a,(hl)
            ld   (ix+4),a
            inc  hl
            ld   a,(hl)
            ld   (ix+5),a
            inc  hl
            ld   (ix+2),l
            ld   (ix+3),h
            jp   rhythm_upd2

l52e3h      inc  hl
            ld   a,(hl)
            cp   0ffh
            jr   z,.skip
            ld   b,a
            add  a,a
            add  a,b
            ld   c,a
            ld   b,0
            ld   l,(ix+0)
            ld   h,(ix+1)
            add  hl,bc
            ld   a,(hl)
            ld   (ix+19),a
            inc  hl
            ld   a,(hl)
            ld   (ix+4),a
            inc  hl
            ld   a,(hl)
            ld   (ix+5),a
            inc  hl
            ld   (ix+2),l
            ld   (ix+3),h
            jp   rhythm_upd2
            ;
.skip       ld   a,PSG_C_AMP
            ld   e,0
            call WRTPSG
            ld   a,(vactive)
            res  6,a            ; reset bit 6 in A
            ld   (vactive),a
            or   a              ; all voices off?
            jp   z,stopbgm      ;   yes: disable BGM H.TIMI hook and return
            ret

unk_data    db   $AE, $00, $B8, $10, $C3, $10, $CE, $10
            db   $DA, $10, $E7, $10, $F5, $10, $03, $11
            db   $12, $11, $22, $11, $34, $11, $46, $11
            db   $AC, $12, $B6, $12, $C1, $12, $CD, $12
            db   $D9, $12, $E6, $12, $F3, $12, $02, $13
            db   $11, $13, $21, $13, $32, $13, $44, $13
            db   $AC, $14, $B6, $14, $C1, $14, $CC, $14
            db   $D8, $14, $E5, $14, $F3, $14, $01, $15
            db   $10, $15, $21, $15, $32, $15, $44, $15
            db   $AB, $16, $B5, $16, $C0, $16, $CC, $16
            db   $D8, $16, $E5, $16, $F2, $16, $01, $17
            db   $10, $17, $20, $17, $31, $17, $43, $17
            db   $AB, $18, $B5, $18, $C0, $18, $CC, $18
            db   $D8, $18, $E5, $18, $F2, $18, $01, $19
            db   $10, $19, $20, $19, $31, $19, $43, $19
            db   $AB, $1A, $B5, $1A, $C0, $1A, $CB, $1A
            db   $D8, $1A, $E4, $1A, $F2, $1A, $00, $1B
            db   $10, $1B, $20, $1B, $31, $1B, $43, $1B
            db   $AB, $1C, $B5, $1C, $C0, $1C, $CB, $1C
            db   $D8, $1C, $E4, $1C, $F2, $1C, $00, $1D
            db   $10, $1D, $20, $1D, $31, $1D, $43, $1D
            db   $AB, $1E, $B5, $1E, $C0, $2E, $CB, $1E
            db   $D8, $1E, $E4, $1E, $F2, $1E, $00, $1F
            db   $10, $1F, $20, $1F, $31, $1F, $43, $1F

;   PSG ch.C is used to generate a noise burst, fading over a short time.
psgCamp     db   $00        ; current amplitude of noise from PSG ch.C
psgCnextstep db  $00        ; ticks remaining before next amplitude step down
psgCampstep db   $00        ; number of ticks between stepping down amplitude


playing     db   $00        ; 0: not playing, original H.TIM hook installed
                            ; 1: playing, our H.TIM hook installed
                            ; Must be initialized to 0! (Otherwise it may copy
                            ; garbage over the current timer hook.)

vactive     db   $00        ; bitmask of PSG noise active (b6) and which of
                            ;   6 FM tone voices that are active (b5-0)
curvoice    db   $00        ; current voice channel we are updating
_53E8       db   $FF

;   Copied to voiceN[4:20].
voice_init  dw   _53E8
            db   $01, $01, $08, $00, $00, $00
            dw   $0000, $0000
            db   $00, $01, $00, $01

; XXX voice0 - voice5 are tone voices? voice6 and 7 drum kit?

                                        ; off.  init   notes
voice0      dw   $0000                  ; ix+0  t00_A
            dw   $0000                  ; ix+2  t00_A
            dw   $0000                  ; ix+4  $53E8
            db   $00                    ; ix+6  $01    countdown
            db   $00                    ; ix+7  $01    countdown
            db   $00                    ; ix+8  $08    # times to double HL
            db   $00                    ; ix+9  $00
            db   $00                    ; ix+10 $00
            db   $00                    ; ix+11 $00
            dw   $0000                  ; ix+12 $0000
            dw   $0000                  ; ix+14 $0000
            db   $00                    ; ix+16 $00
            db   $00                    ; ix+17 $01
            db   $00                    ; ix+18 $00
            db   $00                    ; ix+19 $01

            db   20 dup $00             ; voice1
            db   20 dup $00             ; voice2
            db   20 dup $00             ; voice3
            db   20 dup $00             ; voice4
            db   20 dup $00             ; voice5

voice6      db   3 dup $00              ; voice6
            db   3 dup $00              ; _, ix+4, ix+5
            db         $00              ; ix+6  updated by `rhythm_upd`
            db   9 dup $00
            db         $00              ; ix+16
            db         $00              ; ix+17
            db   2 dup $00

            db  20 dup $00              ; voice7

savedhook   db   5 dup $00  ; A copy of the routine at H.TIMI before we put
                            ; put ours there. Called after we're done to
                            ; continue origina H.TIMI interrupt processing.

instbuf     db   8 dup $00  ; instrument data read with RDDATA
