1 'save"hexdump.bas",a
3 'NOTE: use at least WIDTH 30
5 'S=start address; L=length
6 'Use GOTO 1 to use existing values
9 IF S<>0 GOTO 19
10 S=&H9000
19 IF L<>0 GOTO 30
20 L=&H40
30 GOSUB 1000
99 END
1000 ' hex dump subroutine
1010 FOR A=S TO S+L-1
1020 IF A MOD 8 = 0 THEN PRINT HEX$(A);":";
1030 V=PEEK(A)
1040 PRINT " ";
1050 IF V <= &HF THEN PRINT "0";
1060 PRINT HEX$(V);
1070 IF A MOD 8 = 7 OR A MOD 8 = -1 THEN PRINT
1080 NEXT A
1090 RETURN
