FRIEVE in MSX-FAN Games
=======================

This repo contains reverse-engineering and translation of Japanese MSX
games from the [FRIEVE in MSX-FAN][fr msx-fan] game collection on the
[Frieve / MSX][fr msx] page.

Some of the original games (such as `SHOP.BAS`) appear to run on MSX1
machines; others (such as `SLIMESTA.BAS`) require MSX2. `AUTOEXEC.BAS`
breaks on MSX1 due only to the `COLOR=NEW` in line 10; removing that
makes it work fine on MSX1.

To run programs built from the reverse-engineered source code, you will
need some additional programs. See ["Development
Prerequisites"](#development-prerequisites) below for how to get and
install those, and the documentation for the `Test` script under
["Development"](#development) for how to build and run things.

### Contents

- [Files and Organization](#files-and-organization)
- [Running and Debugging on MSX](#running-and-debugging-on-msx)
- [Development Prerequisites](#development-prerequisites)
- [Development](#development)


Files and Organization
----------------------

While the MSX file system is case-insensitive, our convention is to use
lower-case for all filenames we create in this repo because that is
openMSX's default when creating new files in a host filesystem. Filenames
from original sources maintain their original case.

- `doc/`: Documentation files about individual games.
- `bin/emu`: Run the OpenMSX emulator. (Bash script; see below.)
- `cons/`: Console scripts for use with the `-s` option to `bin/emu`.
- `files/`: The files from the original FRIEVE in MSX-FAN archive
  (`frieve_msxfan.zip`).
- `disk/`: Default working directory for the emulated floppy disk drive.
  No files are committed under this.
- `src/`: Reverse-engineered source code for files from `files/`.
  This may include bug fixes and translated versions of the files.
  For more information, see below.
- `Test`: Script to build and/or run various targets. See below.
- `tool/`: Development tools
  - `tool/bastok/`: BASIC detokeniser (see below).
  - `tool/blextract`: Extract and relocate code fragments from BLOADed
    files so that `bin/msx-dasm` can disassemble them at the runtime
    location. See the program comments for more details.
  - `tool/hexdump.bas`: BASIC hex dump utility; ASCII save.
    (Copy to the drive image dir where you need it and edit as necessary.)


Running and Debugging on MSX
----------------------------

- In openMSX, turning on fast-forward (`F9` on Windows and Linux) can
  greatly speed `LOAD` and `MERGE` commands, espcially with ASCII saves of
  BASIC programs. It will also make key repeat instant; turn it off (with
  `F9` again) if you encounter this.

- Ctrl-STOP or an error during the run of a BASIC game will break out of
  the game but leave the screen in the game graphics character set with the
  cursor in a random location.
  - Before typing a command, press Ctrl-U to clear the screen editor's
    current line, which will usually be the entire screen.
  - Switching to screen 0 or 1 will clear the screen and revert to the
    standard charset; `SCREEN 0:WIDTH 80` is good on MSX2.
  - After switching, you can show the most recent error line, code and
    message with `? ERL,ERR : ERROR ERR`. (Error codes are also listed at
    [ERR].) Line number 65535 (which is not a valid line number) indicates
    an error from a direct command.

- openMSX before 18.0 does not assert the disk changed line when using
  DirAsDSK mounts, so some machines will not see changes you make on
  the host. Reset the emulated machine or load other files to clear
  the MSX-DOS cache.

- If you run into issues saving programs in ASCII format, where even a LOAD
  immediately followed by a `SAVE "…",A` drops most of the program (writing
  out only the first part of it), try restarting openMSX.


Development Prerequisites
-------------------------

### Bash

The scripts here use Bash and so should run without issues on any Unix
system, so long as Bash is installed. On Windows you should use MINGW Bash;
the easiest way to get this (and associated tools) is to install [Git for
Windows][gfw]. When running the scripts, make sure you're using a "Git
Bash" window, not a Windows Console command line.

### openMSX

The `bin/emu` script is used to start the [openMSX] emulator. If the
`openmsx` program can be found using $PATH it will use that, otherwise it
will try some typical installation locations:
- On Unix systems where the openMSX package has been installed, `openmsx`
  will almost invariably be in $PATH. If you build your own copy and don't
  want it in your path, you can install it under
  `/opt/openMSX/bin/openmsx`, or make that a symbolic link to your
  installation directory.
- On Windows the default installation location is `/c/Program Files/openMSX/`
  and `bin/emu` will find it there. If you install it elsewhere you can
  make that a directory junction to your installation location by running
  `mklink /j "C:\Program Files\openMSX" "..."` where "..." is your
  installation location. Note that `mklink` must be run in a Windows
  command line window; it will not work in MINGW Bash.

You will also need appropriate ROM images for the machines you wish to use
(the default is `Sony_HB-F1XD`, or `National_CF-3300` with the `-1` option).
See the [openMSX documentation][omsx-roms] or [these notes][sedoc-msx-emu]
for information on finding and installing those.

### Macro Assembler AS

The `bin/aslmsx` script runs the [Macro Assembler AS][maas] (MAAS) to build
MSX binary code.

On Unix you'll need to build this yourself; the easiest way to do this is
by cloning the [`Macroassembler-AS/asl-releases`][gh-asl] repository from
GitHub and following the instructions there. If `asl` is in your path
(which it typically is if you install it under `/usr/local/`) that will be
used. If you don't want it there, install it in `/opt/MAAS/` or make that a
symlink to your install location.

On Windows [Win32 binaries are available][asl-dl]. If `asw` is in your
path, that will be used, but typically it is not. If you unpack the Win32
binaries under `"/c/Program\ Files (x86)/MAAS\"`, `bin/aslmsx` will find
them there, or you can unpack them anywhere and create a directory junction
to your location with `mklink /j "C:\Program Files (x86)\MAAS" "..."` where
"..." is your installation location. Note that `mklink` must be run in a
Windows command line window; it will not work in MINGW Bash.


Development
-----------

Note that all scripts are Bash scripts. On Unix these can be run from any
shell so long as Bash is also available; on Windows they must be run from a
MINGW Bash window. See "Development Prerequisites" above for details on
what software you need and where to get it.

### Test

The top-level `Test` script has various tasks, or "targets," to build from
source and run original and built code in an emulator. (It uses `bin/emu`
to run the emulator and `bin/aslmsx` to run the assembler; both are
described in more detail below.)

The first parameter to `Test` is the target name; optionally further
parameters may be given, depending on the target. The script itself has a
function for each target; read that to see the list of possible targets and
what they do. some of the more common targets are:

- `./Test files`: Run the original version of the code, mounting the
  `files/` directory as a drive in the emulator.

### bin/emu

`bin/emu` is a Bash script that will run the [openMSX] emulator, mounting the
files under `disk/` on the emulated floppy drive. (Use `bin/emu -d files/`
to start the emulator with the original diskette mounted.) Run `bin/emu -h`
to get further help on the options.

The script assumes you have `openMSX` installed on your machine. On Linux
and Mac it must be in your `$PATH`; on Windows it will find it in your
`%PATH%` or in `C:\Program Files\openMSX\`. (On Windows you also need to
make sure you're using a "Git Bash" window from [Git for Windows][gfw].)

If you can't use the script you can also just run OpenMSX directly;
see the script for hints on command line options.

### Macro Assembler AS (asl)

Version 1.42bld246 or later of the [Macro Assembler AS] is required to
build any machine-language code. This is invoked by `bin/aslmsx`. If that
can find `asl` in the default search path, that will be used, otherwise it
will search for it in a few likely places.

(You can also use an earlier version, but only from your path, if you set
the `ASL_PRE_246_IN_PATH` environment variable to any value.)

For Linux, it's easiest to fetch the latest source from the
[`github.com/Macroassembler-AS/asl-releases`][asl-releases] repository and
built it per the instructions in the README there. If you don't want to
install it in /usr/local or someplace else in your default path, you can
install it under `/opt/asl/` (or symlink that to whatever directory has the
binaries).

For Windows you can download the [WIN32 binary release][asl-win32] and
install it under `C:\Program Files (x86)\asl\`, or any place else if you
add the `asl\bin\` directory to your path. (You may also link the former
directory to your install location with `mklink /j "C:\Program Files
(x86)\asl\" <YOUR-INSTALL>` using a Windows command line—not Bash.) Note
that you'll need at least version 1.42bld246, or you'll need to rename
`asw.exe` to `asl.exe`.

### tmp/

The `tmp/` subdirectory is in the `.gitignore` and can be used for
storage of working data (such as disk image directories for debugging)
that you do not want to commit.

### Submodules

This repo uses [`bastok`][] (MSX BASIC tokenization tools) as a submodule
at `bastok/`. If not cloned with the `--recursive` option, do a `git
submodule update --init` to check out the submodule. It is also a good idea
to run the tests (`bastok/Test`) after the checkout.

### src/

The files under `src/` are "source code" versions (with Unix LF-only line
endings) of some of the binary files under `files/`. Filenames have been
changed to lower case and any `.BAS` files have had their extension changed
to `.ba` to indicate that they are now detokenised UTF-8 versions of BASIC
code.

These files may have reformatting and additional comments from the
reverse-engineering process. English translations will also appear under
here with different names, when we get around to doing that.

Disassemblies (.asm files) that include relocated code sequences are likely
to have incorrect absolute addreses for `ld`, `jp`, etc. instructions. the
`tool/blextract` program can help extract these routines for separate
disassembly at the correct starting address.

When there are many copies of similar routines, such as with the Slime
World music files (four files `files/{BGM,END}-{PSGV,FMVS}.SLM` each with
an initialization routine and two relocated subroutines), we generally
don't commit all the multiple similar copies. Instead we create one
representative copy and annotate that with notes on the differences
between copies. (Often this is just a few different symbol values.)



<!-------------------------------------------------------------------->
[Macro Assembler AS]: http://john.ccac.rwth-aachen.de:8000/as/
[`bastok`]: https://github.com/0cjs/bastok
[asl-releases]: https://github.com/Macroassembler-AS/asl-releases.git
[asl-win32]: http://john.ccac.rwth-aachen.de:8000/as/download.html#WIN32
[fr msx-fan]: https://web.archive.org/web/20160404014555/http://www.frieve.com/msx/frieve_msxfan.zip
[fr msx]: https://web.archive.org/web/20160404014555/http://www.frieve.com/msx.html

<!-- Development Prerequisites -->
[asl-dl]: http://john.ccac.rwth-aachen.de:8000/as/download.html
[gfw]: https://gitforwindows.org/
[gh-asl]: https://github.com/Macroassembler-AS/asl-releases
[maas]: http://john.ccac.rwth-aachen.de:8000/as/
[omsx-roms]: https://openmsx.org/manual/setup.html#downloadrom
[openMSX]: https://openmsx.org/
[sedoc-as]: https://github.com/0cjs/sedoc/blob/master/ee/tools/AS.md
[sedoc-msx-emu]: https://github.com/0cjs/sedoc/blame/master/8bit/msx/emulation.md

<!-- Development -->
[`bastok`]: https://github.com/0cjs/bastok
[maas-doc]: http://john.ccac.rwth-aachen.de:8000/as/as_EN.html
[sedoc-maas]: https://github.com/0cjs/sedoc/blob/master/ee/tools/AS.md
